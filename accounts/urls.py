from django.urls import path
from accounts.views import signup, user_logout, user_login


urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
]
