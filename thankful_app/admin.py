from django.contrib import admin
from thankful_app.models import Thought


# Register your models here.
# allows you to access any of your models in the admin page
@admin.register(Thought)
class ThoughtAdmin(admin.ModelAdmin):
    list_display = (
        "thought",
        "id",
    )
