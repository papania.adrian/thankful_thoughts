from django.apps import AppConfig


class ThankfulAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "thankful_app"
