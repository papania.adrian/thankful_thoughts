from django.forms import ModelForm
from thankful_app.models import Thought


# displays what fields are shown in our form
class ThoughtForm(ModelForm):
    class Meta:
        model = Thought
        fields = ("title", "thought")
