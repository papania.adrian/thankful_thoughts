from django.db import models
from django.contrib.auth.models import User
# this model is used to create a thought instance






class Thought(models.Model):
    title = models.CharField(max_length=200, default="")
    thought = models.TextField(default="")
    created_on = models.DateTimeField("Date for your Thought", blank=True, null=True)
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)