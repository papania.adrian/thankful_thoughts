"""theses are the url patterns for the thankful app"""
from django.urls import path
from thankful_app.views import home_page, show_thoughts, create_thought


urlpatterns = [
    path("home", home_page, name="home_page"),
    path("", show_thoughts, name="show_thoughts"),
    path("create/", create_thought, name="create_thought"),
]
