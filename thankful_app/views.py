from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from thankful_app.models import Thought
from thankful_app.forms import ThoughtForm


@login_required
def home_page(request):
    print("loaded home page")
    return redirect("show_thoughts")


# this function shows a list of thoughts
@login_required
def show_thoughts(request):
    thoughts = Thought.objects.filter(author=request.user)
    print("thoughts:", thoughts)
    context = {
        "thoughts": thoughts,
    }
    return render(request, "thoughts/list.html", context)


# this function lets you create a new thought
@login_required
def create_thought(request):
    if request.method == "POST":
        form = ThoughtForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            return redirect("show_thoughts")

    else:
        form = ThoughtForm()

    context = {"form": form}
    return render(request, "thoughts/create.html", context)
